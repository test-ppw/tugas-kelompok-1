from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from datetime import datetime
from report_form.models import Report

def report_list(request):
	# response = {}
	# reports = Report.objects.all()
	# print(reports)
	# response['report_list'] = reports
	# return render(request, 'report_list/report_list.html', response)
	reports = Report.objects.all()
	return render(request, 'report_list/report_list.html', {'reports': reports})

# def add_report(request):
#     if(request.method == POST):
#         form = ReportForm()
#         reports = Report.objects.all()
#         return render(request, 'report_form/report_form.html', {'form': form, 'reports': reports})
