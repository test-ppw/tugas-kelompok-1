Anggota Kelompok :
1. Irfan Maulana Nasution
2. Hana Fadhila Ardiansyah
3. Timothy Regana Tarigan
4. Tsaqif Naufal

Link Herokuapp : ka15.herokuapp.com/

Cerita :  Aplikasi yg kami buat bernama pacility, dimana app tersebut dapat membantu orang-orang di Universitas Indonesia untuk melaporkan terkait dengan fasilitas (kebersihan, kerusakan, dan sejenisnya) di area Universitas Indonesia.

Fitur:
* Di page home, kami membuat fitur yang bisa membuat sebuah pengumuman, kami membuat text box untuk mengisi status tersebut, kami menggunakan button post lalu status tersebut akan muncul di bawahnya.
* Di page form laporan, kami membuat form inputan untuk user melaporkan keluhan-keluhan yang ada di sekitar UI, di form tersebut akan ada beberapa input form terkait dengan apa yang akan dilaporkan.
* Di page report, kami membuat choices untuk melihat laporan2 yang sudah ada sebelumnya dan dapat di filter dengan beberapa kategori, contohnya area dan waktu.
* Di page testimoni, kami membuat form untuk para user memberikan testimoni.

[![pipeline status](https://gitlab.com/test-ppw/tugas-kelompok-1/badges/master/pipeline.svg)](https://gitlab.com/test-ppw/tugas-kelompok-1/commits/master)